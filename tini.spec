Name:           tini
Version:        0.19.0
Release:        1
Summary:        A tiny but valid init for containers

License:        MIT
URL:            https://github.com/krallin/tini
Source0:        %{url}/archive/v%{version}/%{name}-%{version}.tar.gz

BuildRequires:  cmake
BuildRequires:  gcc
BuildRequires:  glibc-static
BuildRequires:  sed

%description
Tini is the simplest init you could think of.

All Tini does is spawn a single child (Tini is meant to be run in a container),
and wait for it to exit all the while reaping zombies and performing signal
forwarding.

%package        static
Summary:        Standalone static build of %{name}
%description    static
This package contains a standalone static build of %{name}, meant to be used
inside a container.

%prep
%autosetup
# Do not strip binaries
sed -i CMakeLists.txt -e 's/ -Wl,-s//'

%build
%cmake
%__cmake --build . %{?_smp_mflags} --verbose

%install
DESTDIR="%{buildroot}" %__cmake --install .

%files
%license LICENSE
%doc README.md
%{_bindir}/tini

%files static
%license LICENSE
%doc README.md
%{_bindir}/tini-static

%changelog
* Wed Mar 22 2023 lichaoran <pkwarcraft@hotmail.com> - 0.19.0-1
- Initial package
